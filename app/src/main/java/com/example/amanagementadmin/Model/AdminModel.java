package com.example.amanagementadmin.Model;

public class AdminModel {
    String account, adminname, avt, contact, password;

    public AdminModel() {
    }

    public AdminModel(String account, String adminname, String avt, String contact, String password) {
        this.account = account;
        this.adminname = adminname;
        this.avt = avt;
        this.contact = contact;
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAdminname() {
        return adminname;
    }

    public void setAdminname(String adminname) {
        this.adminname = adminname;
    }

    public String getAvt() {
        return avt;
    }

    public void setAvt(String avt) {
        this.avt = avt;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
