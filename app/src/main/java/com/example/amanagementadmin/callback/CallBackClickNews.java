package com.example.amanagementadmin.callback;

import android.view.View;

public interface CallBackClickNews {
    void OnNewsClickListener (View view, int pos);
}
