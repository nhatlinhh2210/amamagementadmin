package com.example.amanagementadmin.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amanagementadmin.Model.NewsModel;
import com.example.amanagementadmin.R;
import com.example.amanagementadmin.adapter.NewsAdapter;
import com.example.amanagementadmin.callback.CallBackNews;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.recycler_news)
    RecyclerView recycler_news;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, root);
        init();
        homeViewModel.getMutableLiveData().observe(this, newsModels -> {
            NewsAdapter newsAdapter = new NewsAdapter(getContext(), newsModels);
            recycler_news.setAdapter(newsAdapter);
        });
        return root;
    }

    private void init() {
        recycler_news.setHasFixedSize(true);
        recycler_news.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
    }
}