package com.example.amanagementadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.amanagementadmin.Common.Common;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.example.amanagementadmin.Model.AdminModel;

public class Login extends AppCompatActivity {

    // Khai bao bien
    Button btnLogin;
    EditText account, password;

    // Khai bao data
    DatabaseReference adminRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Lien ket voi xml
        linkView();

        // Ket noi database
        adminRef = FirebaseDatabase.getInstance("https://amamagement-478ec-default-rtdb.firebaseio.com/").getReference(Common.ADMIN_REFERENCE);

        // Xu ly su kien dang nhap
        addEvent();
        // ID Pass:  admin1 - 192a

    }

    private void addEvent() {
        // Su kien nut login
        btnLogin.setOnClickListener(v -> {
            // Goi ham check
            checkInput();

            // Kiem tra tai khoan va dang nhap
            adminRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //kiem tra tai khoan trong database
                    if (dataSnapshot.child(account.getText().toString()).exists()) {
                        //lay thong tin nguoi dung
                        AdminModel user = dataSnapshot.child(account.getText().toString()).getValue(AdminModel.class);
                        if (user.getPassword().equals(password.getText().toString())) {
                            adminRef.child(user.getAccount()).setValue(user)
                                    .addOnCompleteListener(task -> {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(Login.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                                            gotohomeactivity(user);
                                        }
                                    });
                        } else {
                            Toast.makeText(Login.this, "Sai mật khẩu", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Tài khoản không tồn tại", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled( DatabaseError databaseError) {
                    Toast.makeText(Login.this, "DatabaseError", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    // Chuyen toi Activity Home
    private void gotohomeactivity(AdminModel user) {
        Common.currentAdmin = user;
        startActivity(new Intent(Login.this, MainActivity.class));
    }

    // Kiem tra du lieu nhap vao Edit text
    private void checkInput() {
        if (account.equals("")){
            Toast.makeText(this, "Vui lòng nhập tài khoản", Toast.LENGTH_SHORT).show();
        }
        if (password.equals("")){
            Toast.makeText(this, "Vui lòng nhập mật khẩu", Toast.LENGTH_SHORT).show();
        }
    }

    private void linkView() {
        btnLogin = findViewById(R.id.btnLogin);
        account = findViewById(R.id.etID);
        password = findViewById(R.id.etPass);
    }
}